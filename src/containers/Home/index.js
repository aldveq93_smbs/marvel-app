import React from 'react';
import logo from '../../assets/images/marvel.svg';

const Home = () => {
    return(
        <div className="home-section animate__animated animate__fadeIn">
            <img src={logo} className="home-section__logo" alt="Marvel App" />
            <p>
                Welcome to the Marvel App!
            </p>
        </div>
    );
}

export default Home;