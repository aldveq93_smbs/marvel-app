import React, { useCallback, useEffect, useState } from 'react';
import { Loader, MarvelItemDetails } from '../../components';
import { useParams } from 'react-router';
import { ThirdService } from '../../utilities/third-service';

const SingleCharacter = () => {

    const { id } = useParams();
    const [marvelCharacter, setMarvelCharacter] = useState({});
    const [marvelCharacterComics, setMarvelCharacterComics] = useState([]);
    const [marvelCharacterStories, setMarvelCharacterStories] = useState([]);
    const [loading, setLoading] = useState(false);
    
    const getMarvelCharacterById = useCallback(async () => {
        const marvelService = new ThirdService();
        setLoading(true);

        const marvelCharacterFromService = await marvelService.getCharacterById(id);

        setMarvelCharacter(marvelCharacterFromService);
        setMarvelCharacterComics(marvelCharacterFromService?.comics?.items);
        setMarvelCharacterStories(marvelCharacterFromService?.stories?.items);
        
        setLoading(false);
    }, [id, setLoading, setMarvelCharacter, setMarvelCharacterComics, setMarvelCharacterStories]);

    useEffect(() => {
        getMarvelCharacterById();
    }, [getMarvelCharacterById]);

    const drawSingleCharacter = () => {
        return (
            <MarvelItemDetails marvelDataDetails={marvelCharacter} listOne={marvelCharacterComics} listTwo={marvelCharacterStories} titleListOne='Comics' titleListTwo='Stories' urlListOne='comics' urlListTwo='stories' />
        );
    }

    return (
        <>
            { loading ? <Loader/> : drawSingleCharacter() }
        </>
    );
}

export default SingleCharacter;