import React, { useCallback, useEffect, useState } from 'react'
import { Loader, MarvelItemDetails } from '../../components';
import { useParams } from 'react-router';
import { ThirdService } from '../../utilities/third-service';

const SingleStory = () => {

    const { id } = useParams();
    const [loading, setLoading] = useState(false);
    const [storyData, setStoryData] = useState({});
    const [storyCharacters, setStoryCharacters] = useState([]);
    const [storyComics, setStoryComics] = useState([]);

    const getStoryDataById = useCallback(async () => {
        const marvelService = new ThirdService();
        setLoading(true);
        const storyDataFromMarvel = await marvelService.getStoryById(id);
        setStoryData(storyDataFromMarvel);
        setStoryCharacters(storyDataFromMarvel.characters.items);
        setStoryComics(storyDataFromMarvel.comics.items);
        setLoading(false);
    },[id, setLoading]);

    useEffect(() => {
        getStoryDataById();
    }, [getStoryDataById]);

    const drawStory = () => {
        if(loading)
            return <Loader/>

        return (
            <MarvelItemDetails marvelDataDetails={storyData} listOne={storyCharacters} listTwo={storyComics} titleListOne='Characters' titleListTwo='Comics' urlListOne='characters' urlListTwo='comics' />
        );
    }

    return (
       drawStory()
    )
}

export default SingleStory;