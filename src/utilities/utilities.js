const getMarvelTitleOrName = marvelObj => {
    const existCharacterName = 'name' in marvelObj;
    const existComicTitle = 'title' in marvelObj;

    const characterName = existCharacterName ? marvelObj?.name : '';
    const comicTitle = existComicTitle ? marvelObj?.title : ''; 

    if(characterName !== '')
        return characterName;
        
    return comicTitle;
}

const getMarvelImage = marvelObj => {
    if (marvelObj.thumbnail !== undefined && marvelObj.thumbnail !== null)
        return `${marvelObj?.thumbnail?.path}.${marvelObj?.thumbnail?.extension}`;

    return 'https://i.annihil.us/u/prod/marvel/i/mg/b/40/image_not_available.jpg';
}

export {
    getMarvelTitleOrName,
    getMarvelImage
}