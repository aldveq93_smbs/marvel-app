const marvelPublicKey = process.env.REACT_APP_MARVEL_PUBLIC_KEY;
const marvelPrivateKey = process.env.REACT_APP_MARVEL_PRIVATE_KEY;
const marvelRequestDataLimit = process.env.REACT_APP_MARVEL_REQUEST_DATA_LIMIT; 

export {
    marvelPublicKey,
    marvelPrivateKey,
    marvelRequestDataLimit
}