import axios from 'axios';
import { MD5 } from 'crypto-js';
import { marvelPublicKey, marvelPrivateKey, marvelRequestDataLimit } from './config';

export class ThirdService {
    constructor() {
        this.axios = this.createInstance();
        this.ts = Date.now();
        this.hashText = `${this.ts}${marvelPrivateKey}${marvelPublicKey}`;
        this.requestLimit = marvelRequestDataLimit;
    }

    createInstance() {
        const axiosInstance = axios.create({
            baseURL: "https://gateway.marvel.com/v1/public"
        });
      
        return axiosInstance;      
    }

    async getCharacters() {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/characters?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&limit=${this.requestLimit}`);
            return {
                total: res.data.data.total,
                data: res.data.data.results
            };
        } catch(error) {
            return { error };
        }
    }

    async getCharactersPagination(offset) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/characters?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&limit=${this.requestLimit}&offset=${offset}`);
            return res.data.data.results;
        } catch(error) {
            return { error }
        }
    }

    async getComicsPagination(offset) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/comics?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&limit=${this.requestLimit}&offset=${offset}`);
            return res.data.data.results;
        } catch(error) {
            return { error };
        }
    }

    async getStoriesPagination(offset) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/stories?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&limit=${this.requestLimit}&offset=${offset}`);
            return res.data.data.results;
        } catch(error) {
            return { error };
        }
    }

    async getCharacterById(id) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/characters/${id}?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}`);
            return res.data.data.results[0];
        } catch(error) {
            return { error };
        }
    }

    async getCharactersByName(characterName) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/characters?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&nameStartsWith=${characterName}&limit=${this.requestLimit}`);
            return {
                total: res.data.data.total,
                data: res.data.data.results
            };
        } catch(error) {
            return { error };
        }
    }

    async getCharactersByNamePagination(characterName, offset) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/characters?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&nameStartsWith=${characterName}&limit=${this.requestLimit}&offset=${offset}`);
            return res.data.data.results;
        } catch(error) {
            return { error };
        }
    }

    async getCharacterByComics(comicsData) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/characters?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&comics=${comicsData}&limit=${this.requestLimit}`);
            return {
                total: res.data.data.total,
                data: res.data.data.results
            };
        } catch(error) {
            return { error };
        }
    }

    async getCharactersByComicPagination(comicsData, offset) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/characters?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&comics=${comicsData}&limit=${this.requestLimit}&offset=${offset}`);
            return res.data.data.results;
        } catch(error) {
            return { error };
        }
    }

    async getCharactersOrderedByName() {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/characters?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&orderBy=-name&limit=${this.requestLimit}`);
            return {
                total: res.data.data.total,
                data: res.data.data.results
            };
        } catch(error) {
            return { error };
        }
    }

    async getCharactersOrderedByNamePagination(offset) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/characters?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&orderBy=-name&limit=${this.requestLimit}&offset=${offset}`);
            return res.data.data.results;
        } catch(error) {
            return { error };
        }
    }

    async getComics() {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/comics?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&limit=${this.requestLimit}`);
            return {
                total: res.data.data.total,
                data: res.data.data.results
            };
        } catch(error) {
            return {error};
        }
    }

    async getStories() {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/stories?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&limit=${this.requestLimit}`);
            return {
                total: res.data.data.total,
                data: res.data.data.results
            };
        } catch(error) {
            return {error};
        }
    }

    async getStoryById(storyId) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/stories/${storyId}?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}`);
            return res.data.data.results[0];
        } catch(error) {
            return {error};
        }
    }

    async getCharacterByStories(storiesData) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/characters?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&stories=${storiesData}&limit=${this.requestLimit}`);
            return {
                total: res.data.data.total,
                data: res.data.data.results
            };
        } catch(error) {
            return {error};
        }
    }

    async getCharactersByStoryPagination(storiesData, offset) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/characters?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&stories=${storiesData}&limit=${this.requestLimit}&offset=${offset}`);
            return res.data.data.results;
        } catch(error) {
            return { error };
        }
    }

    async getComicsByFormat(format) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/comics?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&format=${format}&limit=${this.requestLimit}`);
            return {
                total: res.data.data.total,
                data: res.data.data.results
            };
        } catch(error) {
            return {error};
        }
    }

    async getComicsByFormatPagination(format, offset) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/comics?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&format=${format}&limit=${this.requestLimit}&offset=${offset}`);
            return res.data.data.results;
        } catch(error) {
            return { error };
        }
    }

    async getComicsByTitle(comicTitle) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/comics?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&titleStartsWith=${comicTitle}&limit=${this.requestLimit}`);
            return {
                total: res.data.data.total,
                data: res.data.data.results
            };
        } catch(error) {
            return { error }
        }
    }

    async getComicsByTitlePagination(comicTitle, offset) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/comics?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&titleStartsWith=${comicTitle}&limit=${this.requestLimit}&offset=${offset}`);
            return res.data.data.results;
        } catch(error) {
            return { error };
        }
    }

    async getComicsByIssueNumber(issueNumber) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/comics?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&issueNumber=${issueNumber}&limit=${this.requestLimit}`);
            return {
                total: res.data.data.total,
                data: res.data.data.results
            };
        } catch(error) {
            return { error };
        }
    }

    async getComicsByIssueNumberPagination(issueNumber, offset) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/comics?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&issueNumber=${issueNumber}&limit=${this.requestLimit}&offset=${offset}`);
            return res.data.data.results;
        } catch(error) {
            return { error };
        }
    }

    async getComicsOrderedByIssueNumber() {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/comics?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&orderBy=issueNumber&limit=${this.requestLimit}`);
            return {
                total: res.data.data.total,
                data: res.data.data.results
            };            
        } catch (error) {
            return { error };
        }
    }

    async getComicsOrderedByIssueNumberPagination(offset) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/comics?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}&orderBy=issueNumber&limit=${this.requestLimit}&offset=${offset}`);
            return res.data.data.results;
        } catch (error) {
            return { error };
        }
    }

    async getComicById(comicId) {
        try {
            const hash = this.getHash();
            const res = await this.axios.get(`/comics/${comicId}?ts=${this.ts}&apikey=${marvelPublicKey}&hash=${hash}`);
            return res.data.data.results[0];
        } catch (error) {
            return { error };
        }
    }
    
    getHash() {
        return MD5(this.hashText);
    }

}