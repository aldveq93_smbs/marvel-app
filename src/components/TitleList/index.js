import React from "react";
import TitleItem from "../TitleItem";

const TitleList = ({title, data, url}) => {
    
    if(data.length === 0)
        return <p className="title-list-data-container-no-data-text">{`No available ${title.toLowerCase()} :(`}</p>
    
    const itemsData = data.map((mvData, mvIndex) => <TitleItem key={`${mvData.name}-${mvIndex}`} urlDestination={url} itemTitle={mvData.name} resourceUri={mvData.resourceURI}/>);
    return(
        <div className="title-list-data-container">
            <h4 className="title-list-data-container__title">{title}</h4>
            <ul className="title-list-data-container__list">{itemsData}</ul>
        </div>
    );
}

export default TitleList;