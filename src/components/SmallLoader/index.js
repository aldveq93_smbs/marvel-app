import React from 'react'

const SmallLoader = () => {
    return (
        <div className="small-loader-container">
            <div className="lds-ellipsis"><div></div><div></div><div></div><div></div></div>
        </div>
    )
}

export default SmallLoader;