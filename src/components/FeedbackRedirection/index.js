import React from 'react'
import { Link } from 'react-router-dom';

const FeedbackRedirection = ({feedbackType, message, urlRedirection}) => {

    const type = feedbackType === 'success' ? 'success' : 'error'; 

    return (
        <>
            <div className={`feedback-container ${type} animate__animated animate__fadeIn`}>
                <h2>{message}</h2>
            </div>
            <Link to={urlRedirection} className="feedback-container__button animate__animated animate__fadeIn">Go back</Link>
        </>
    )
}

export default FeedbackRedirection;