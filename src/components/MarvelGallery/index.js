import React from 'react'
import DztImageGalleryComponent from 'reactjs-image-gallery';

const MarvelGallery = ({ marvelGalleryName, marvelGalleryData }) => {
    return (
        <>
            <h3 className="marvel-gallery-title">{marvelGalleryName}</h3>
            <DztImageGalleryComponent images={marvelGalleryData}/>
        </>
    )
}

export default MarvelGallery;