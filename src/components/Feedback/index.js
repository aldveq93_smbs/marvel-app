import React from 'react';

const Feedback = ({feedbackType, message, getMarvelData, setError}) => {

    const type = feedbackType === 'success' ? 'success' : 'error'; 

    const goBack = () => {
        setError({active: false, message: ''});
        getMarvelData();
    }

    return(
        <>
            <div className={`feedback-container ${type} animate__animated animate__fadeIn`}>
                <h2>{message}</h2>
            </div>
            <button className="feedback-container__button animate__animated animate__fadeIn" onClick={ goBack }>Go back</button>
        </>
    );
}

export default Feedback;